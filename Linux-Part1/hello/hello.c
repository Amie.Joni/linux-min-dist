#include<stdio.h>
#include<stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

extern ssize_t getline(char **lineptr, size_t *n, FILE *stream);

int main(){
  char* line=NULL;
  size_t len=0;
  ssize_t count=0;

  // wait 2 seconds, so that the kernel can finish printing stuff.
  // indeed, some of the boot process is asynchronous and happens
  // after the initial process has been started.
  sleep(2);

  // Be polite, ask for the user name...
  printf("Yo!! tu es : ");
  fflush(stdout);
  
  while (1) {
    count = getline(&line,&len,stdin);
    if (count==-1) {
      printf("\n\nAu revoir.\n");
			printf("Je quitte...\n\n\n");
			fflush(stdout);
      return -1;
    }
    if (count>1)
      break;
    free(line);
    line = NULL;
    len = 0;
  }
  line[count-1]='\0';
  printf("Bienvenue %s,\n",line);
  printf("Pour terminer cette conversation entrer \"quitte\".\n");
    
  // Now enter parrot mode...
  while(1){
    free(line);
    line = NULL;
    len = 0;
    if (count>1)
      printf("T'as dit quoi? ");
    else 
      printf("Quoi ? ");
    fflush(stdout);
    count = getline(&line,&len,stdin);
    if (count==-1) {
      printf("\n\nAu revoir.\n");
			printf("Au revoir...\n\n\n");
			fflush(stdout);
      return -1;
    }
    if (count>1) {
      line[count-1]='\0';
			if (0==strcmp(line,"quitte")) {
				printf("\n\nRavie de te connaitre!\n\n");
				printf("Au revoir...\n\n\n");
				fflush(stdout);
				return 0;
			}
			else
				printf("Tu as dit: %s, mmm alors.\n",line);
    }
  }
}
