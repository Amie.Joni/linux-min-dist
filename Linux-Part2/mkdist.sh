#!/bin/bash
# This shell script creates a bootable Qemu disk, in a file.
# First, it creates a disk image (raw) and sets up GRUB.
# Then, it tweaks with the MBR to create a partition
# and makes an ext2 file system on it.
# Finally, it mounts that partition and copies your mini
# distribution.
#
# This script will show you how to do the following:
#  - use dd
#  - use fdisk & parted
#  - control a program through a script (redirection)
#  - use losetup to mount a file as a disk partition
#    or use mount with -o loop option.

GRUB_DIR=../Linux-Part1/grub-0.97
DIST_DIR=./MiniDist
MOUNT_DIR=/mnt/minidist
DISK=disk.img

LINUX_VER=4.4.113
LINUX_SRC=../linux-$LINUX_VER

# Color definitions for colored output via echo.
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
NOCOLOR='\033[0m'

#-----------------------------------------------------------
# Function to check if the last command was successful,
# it is called with the line number at the check was done.
#-----------------------------------------------------------
function check() {
		if [ $? -ne 0 ] ; then
				echo -e -n "${RED}"
				echo "PANIC: line $1"
				# attempt to clean the loop device if it was created.
				if [ ! -z $DEVLOOP ] ; then
						echo "Cleaning loop device $DEVLOOP"
						mount | grep $DEVLOOP >> /dev/null 2>&1
						if [ $? -eq 0 ] ; then
								sudo umount $DEVLOOP
						fi
						sudo losetup -d $DEVLOOP
				fi		
				echo -e -n "${NOCOLOR}"
				exit 1
		fi  
}

echo "======================================================================"
echo "Creating the disk image..."

# Create the raw disk image...
dd if=/dev/zero of=$DISK bs=512 count=256000 seek=256
check $LINENO

echo "======================================================================"
echo "Partitioning the disk image..."

# Create an msdos label, always the first step in creating a disk.
parted -s $DISK mklabel msdos

# Create a primary parttion, starting at sector 256, taking all the
# remaining sectors.
parted -s $DISK mkpart primary ext2 256s 256000s
check $LINENO

echo "======================================================================"
echo "Loop mount the disk image..."

# Grab available loop device
DEVLOOP=`losetup -f`
check $LINENO

sudo losetup -o131072 $DEVLOOP $DISK
check $LINENO

# the above command could have been written 
#     $ sudo losetup -o$((256*512)) /dev/loop1 $DISK
# since 256*512=131072

echo "======================================================================"
echo "Creating the file system..."

# create a file system in our partition, using the ext2 format
sudo mkfs -t ext2 $DEVLOOP
check $LINENO

# mounting that file system
sudo mkdir -p $MOUNT_DIR
check $LINENO
sudo mount $DEVLOOP $MOUNT_DIR
check $LINENO

#----------------------------------------------------------------------------
# If there is a kernel and modules built
# Copy the kernel and the modules to our minimal distribution

echo "======================================================================"
echo "Grab the newly compiled Linux kernel and modules..."

if [ -e $LINUX_SRC/arch/x86/boot/bzImage ] ; then

		cp $LINUX_SRC/arch/x86/boot/bzImage $DIST_DIR/boot/vmlinuz-$LINUX_VER
		check $LINENO
		[ -e $LINUX_SRC/System.map ] && \
				cp $LINUX_SRC/System.map $DIST_DIR/boot/System.map-$LINUX_VER
		check $LINENO
		
		rm -rf $DIST_DIR/lib/modules
		mkdir -p $DIST_DIR/lib/modules/$LINUX_VER/kernel
		check $LINENO

		pushd $LINUX_SRC
		MODULES=`find . -name "*.ko" -print`
		check $LINENO
		popd
		if [ ! -z "$MODULES" ] ; then 
				echo "  found the following modules: $MODULES"
				for mod in $MODULES ; do
						dir=$(dirname "${mod}")
						mkdir -p $DIST_DIR/lib/modules/$LINUX_VER/kernel/$dir
						check $LINENO
						cp $LINUX_SRC/$mod $DIST_DIR/lib/modules/$LINUX_VER/kernel/$mod
						check $LINENO
				done
				depmod -ae -F $DIST_DIR/boot/System.map-$LINUX_VER -b $DIST_DIR $LINUX_VER
				check $LINENO
		fi
fi

echo "======================================================================"
echo "Copying the mini-dist contents..."

# Now copy the mini distribution onto the partition we just mounted.
# Pay attention at how we use tar and not a copy to preserve
# symbolic links, ownership, and access rights.

( cd $DIST_DIR ; sudo tar -cf - . | (cd $MOUNT_DIR ; sudo tar -xf - ))
check $LINENO

# The previous command could have been written:
#   ( cd $DIST_DIR ; sudo cp -ar . $MOUNT_DIR )


echo "======================================================================"
echo "Syncing the mounted file systems, "
echo "before unmounting and destructing our loop device..."

# Sync the file system buffers with the underlying devices:
sync

# Unmount our partition and free the loop device we created
sudo umount $DEVLOOP
check $LINENO
sudo losetup -d $DEVLOOP
check $LINENO

echo "======================================================================"
echo "Installing GRUB..."

# Create a temporary device map
DEVICE_MAP=/tmp/device.map
rm -f $DEVICE_MAP
cat >> $DEVICE_MAP <<EOF
(hd0) $DISK
EOF
check $LINENO

# Now, we need to start the GRUB shell, using the device map
# we just generated. By re-directing the input, we can automate
# the install, hoping there will be no errors.

$GRUB_DIR/grub  --device-map=$DEVICE_MAP <<EOF
root (hd0,0)
setup (hd0)
quit
EOF
check $LINENO

# Remove our temporary device map
rm -f $DEVICE_MAP

echo "======================================================================"
echo "Booting QEMU..."

NET="-net nic -net user -net dump,file=/tmp/qemu.net"
#NET="-netdev user,id=eth,dns=10.0.2.3 -device e1000,netdev=eth"

qemu-system-i386 -machine pc -cpu coreduo -m 256 -serial mon:stdio -drive format=raw,file=$DISK $NET

exit 0

